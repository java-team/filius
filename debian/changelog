filius (2.7.1+ds-3) UNRELEASED; urgency=medium

  * Fix AppStream MetaInfo data

 -- Andreas B. Mundt <andi@debian.org>  Tue, 17 Dec 2024 16:40:14 +0100

filius (2.7.1+ds-2) unstable; urgency=medium

  * Update debian/copyright
  * Use default-jdk-headless for less dependencies
  * Fix MetaInfo file name (thanks lintian)

 -- Andreas B. Mundt <andi@debian.org>  Mon, 16 Dec 2024 16:56:30 +0100

filius (2.7.1+ds-1) unstable; urgency=medium

  * New upstream version 2.7.1+ds

 -- Andreas B. Mundt <andi@debian.org>  Sat, 14 Dec 2024 13:11:43 +0100

filius (2.6.1+ds-1) unstable; urgency=medium

  * Fix watch file
  * New upstream version 2.6.1+ds

 -- Andreas B. Mundt <andi@debian.org>  Sun, 25 Aug 2024 11:03:55 +0200

filius (2.5.1+ds-2) unstable; urgency=medium

  * Fix uscan for latest gitlab changes.
  * Bump Standards-Version to 4.7.0 (no changes needed).
  * Use at least Java 17 as it is needed for latest filius.

 -- Andreas B. Mundt <andi@debian.org>  Wed, 29 May 2024 10:28:37 +0200

filius (2.5.1+ds-1) unstable; urgency=medium

  * New upstream version 2.5.1+ds.
  * Update patches, remove those applied upstream.
  * Update dependencies and adapt maven rules.
  * Rework package description.
  * Fix FTBFS: Patch exceptions back that have been removed upstream.

 -- Andreas B. Mundt <andi@debian.org>  Sun, 29 Oct 2023 13:22:04 +0100

filius (2.4.2~git20230806.eefa4ee3+ds-4) unstable; urgency=medium

  * Add dependency on runtime environment.

 -- Andreas B. Mundt <andi@debian.org>  Thu, 31 Aug 2023 22:46:37 +0200

filius (2.4.2~git20230806.eefa4ee3+ds-3) unstable; urgency=medium

  * Add upstream metadata to package.
  * Include English introductory documentation.

 -- Andreas B. Mundt <andi@debian.org>  Thu, 24 Aug 2023 10:16:25 +0200

filius (2.4.2~git20230806.eefa4ee3+ds-2) unstable; urgency=medium

  * Split source and general Debian information in separate READMEs.
  * Fix lintian warning 'classpath-contains-relative-path'.
  * Convert markdown docs to HTML and register manual with doc-base.
  * Move configuration to conffile.

 -- Andreas B. Mundt <andi@debian.org>  Sun, 13 Aug 2023 19:48:55 +0200

filius (2.4.2~git20230806.eefa4ee3+ds-1) unstable; urgency=medium

  * Initial release (Closes: #982648)

 -- Andreas B. Mundt <andi@debian.org>  Sun, 06 Aug 2023 22:58:15 +0200
